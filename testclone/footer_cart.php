
<div class="footer_cart" id="footer_cart">
    <div class="summary_ p-2">
        <div class="row">
            <div class="col-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="select_all">
                    <label class="form-check-label" for="select_all" style="font-size: smaller; font-weight:400;">เลือกทั้งหมด</label>
                </div>
            </div>
            <div class="col-8">
                <div class="row">
                    <div class="col-8 m-0 p-0 text-right">
                        <p class="mb-0 txt-list-sumary">จำนวน (รายการ) :</p>
                    </div>
                    <div class="col-4 text-right">
                        <p class="mb-0 txt-list-number" id="sum_qty">0</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8 m-0 p-0 text-right">
                        <p class="mb-0 txt-list-sumary">รวมทั้งสิ้น หยวน(¥) :</p>
                    </div>
                    <div class="col-4 text-right">
                        <p class="mb-0 txt-list-number" id="price_y">0</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8 text-right m-0 p-0">
                        <p class="mb-0 txt-list-sumary" >ราคาประมาณ (แบบคิดตามจริง) :</p>

                        <input type="hidden" name="rate_general[]" class="rate_general" total-start="20000" total-end="999999" value="5.01" />
                        <input type="hidden" name="rate_general[]" class="rate_general" total-start="10000" total-end="19999" value="5.02" />
                        <input type="hidden" name="rate_general[]" class="rate_general" total-start="2000" total-end="9999" value="<?php echo $ratec; ?>" />
                        <input type="hidden" name="rate_general[]" class="rate_general" total-start="0" total-end="1999" value="<?php echo $ratec; ?>" />
                        <input type="hidden" name="rate_express" id="rate_express" value="<?php echo $ratec; ?>" />    
                        <input type="hidden" id="rate_fix" name="rate_fix" value="<?php echo $ratec?>">


                        <input type="hidden" name="get_item" id="get_item" value="0">
                        <input type="hidden" name="get_rate_fix" id="get_rate_fix" value="0">
                        <input type="hidden" name="get_rate_general" id="get_rate_general" value="0">
                        <input type="hidden" name="get_rate_express" id="get_rate_express" value="0">
                    </div>
                    <div class="col-4 text-right">
                        <p class="mb-0 txt-list-number" id="price_t">0</p>
                    </div>
                </div>
                <div class="row d-none">
                    <div class="col-8 text-right m-0 p-0">
                        <p class="mb-0 txt-list-sumary" >ราคาประมาณ (แบบ Fix) :</p>
                        
                    </div>
                    <div class="col-4 text-right">
                        <p class="mb-0 txt-list-number" id="select-total-fix">0</p>
                    </div>
                </div>
                <div class="row d-none">
                    <div class="col-8 text-right m-0 p-0">
                        <p class="mb-0 txt-list-sumary" >ราคาประมาณ (แบบช่องทางด่วน) :</p>
                       
                    </div>
                    <div class="col-4 text-right">
                        <p class="mb-0 txt-list-number" id="select-total-general">0</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="p-2 text-center" id="footer_cart_menu">
        <!-- <form action="#" method="post"> -->
            <center>
                <button type="button" onclick="submitcart()" class="btn btn-warning form-control" id="sub_cart">สั่งซื้อสินค้า</button>
            </center>
        <!-- </form> -->
    </div>
    
</div>
<script>
    function checkstore(count,pro_id) {
        console.log(pro_id);
        $("#shop_name-"+count).on("change", function(e) {
            if ($(this).is(":checked")) {
                $(".pro-st-"+pro_id).prop("checked", true);
                calurate();
            }else{
                $(".pro-st-"+pro_id).prop("checked", false);
                calurate();
            }
  
        });
       
        // console.log("count=>"+count_t);
    }
    function submitcart() {
        var check = false;
        $(".check_pro").each(function() {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                check = true;
            }
            console.log(check);
        });
        if (check) {
            $("#formBasketSave").submit();
        } else {
            alert("กรุณาเลือกสินค้าด้วยค่ะ");
        }
    }
   
    $("#select_all").on("change", function(e) {
    var item = 0;
    var item_pro = 0;
    if ($(this).is(":checked")) {
        $(".check_store").each(function(){
        if(item < 100){
            $(this).prop("checked", true);
            item++;
        }
        });
        
        $(".check_pro").each(function(){
            if(item_pro < 100){
            $(this).prop("checked", true);
            item_pro++;
        }
        });
        //  $(".chkItem").prop("checked", true);
    } else {
        $(".check_store").prop("checked", false);
        $(".check_pro").prop("checked", false);
    }
    // selectBaskets();
    calurate();
    });
    function calurate() {
        var totalGeneral = 0;
        var totalExpress = 0;
        var totalFix = 0;
        var rateGeneral = 0;
        var rateExpress = parseFloat($("#rate_express").val());
        var rateFix = parseFloat($("#rate_fix").val());
        //   var rateFix = parseFloat($("#rate_fix").val());
        var sum = 0;
        var total_y = 0;
        var total = 0;
        var price = 0;
        var item = 0;
        // var rate = 0;
    $(".check_pro").each(function(index) {
        var sThisVal = (this.checked ? $(this).val() : "");

        if (sThisVal && item < 100) {
           
            
            var listddd = $(this).closest('.card-body').find('.sumprice_val');
            price = parseFloat($(listddd).val());
            total += price;
            console.log(price);
            item++;
            console.log("listddd=>"+listddd)
            console.log(sThisVal,item);
        }else if(sThisVal && item > 99){
            $(this).prop("checked", false);
            alert('สามารถทำได้ครั้งละ 100 รายการเท่านั้น');
        }
        
    });

    $(".rate_general").each(function() {
    var start = parseFloat($(this).attr('total-start'));
    var end = parseFloat($(this).attr('total-end'));
    // console.log(start,end,total);
    if(start <= total && total <= end){
      rateGeneral = $(this).val();
    }
  });

  totalGeneral = (parseFloat(total) * rateGeneral);
  totalExpress = (parseFloat(total) * rateExpress);
  totalFix = (parseFloat(total) * rateFix);
  console.log("rategen=>"+rateGeneral);
  console.log("rategex=>"+rateExpress);
  console.log("rategfix=>"+rateFix);

  $('#get_rate_fix').val(rateFix);
  $('#get_rate_general').val(rateGeneral);
  $('#get_rate_express').val(rateExpress);

        $("#sum_qty").text(item);
        $("#sum_qty_vql").val(item);
        $("#price_y").text(numberWithCommas((total).toFixed(2)));
        $("#price_t").text(numberWithCommas(parseFloat(totalGeneral).toFixed(2)));
        $('#select-total-express').text(numberWithCommas(parseFloat(totalExpress).toFixed(2)));
        $('#select-total-fix').text(numberWithCommas(parseFloat(totalFix).toFixed(2)));
        
    }
    function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
</script>